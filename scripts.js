/**
 * 
 * icon-heart
 * icon-comment
 * icon-bookmark
 * 
 */

var iconHeart = document.querySelector('#icon-heart');


/**
 * iconHeart.classList.add('far'); <-- Añadir clase
 * iconHeart.classList.remove('far'); <-- Remover clase
 * iconHeart.classList.toggle('far'); <-- Añadir si no existe y remover si existe la clase
 */

iconHeart.addEventListener('click', function () {
  iconHeart.classList.toggle('far');
  iconHeart.classList.toggle('fas');
});

/** Comments */
var commentContainer = document.querySelector('#comment-container');
var commentInput = document.querySelector('#comment-input');
var userName = 'Talo';


commentInput.addEventListener('keydown', function (event) {
  if (event.key === 'Enter') {
    var commentText = commentInput.value;
    var previousComment = commentContainer.innerHTML;
    commentContainer.innerHTML = previousComment + '<p class="comment">' + 
    '<b>' + userName + '</b>' + ' ' + commentText + '</p>';
    commentInput.value = '';
  }
});

// Cambiar color a comentarios

var comments = document.querySelectorAll('.comment');


function commentClick(comment) {
  comment.style.fontSize;
  comment.style.color = 'red';
}

comments.forEach(function(element) {
  element.addEventListener('click', function () { commentClick(element); });
});

// - revisar que la tecla sea 'enter' if (e.key === 'Enter') { do something }
// - si la tecla es enter obtener el valor del input commentInput.val
// - agregar el valor del input al contendor innerHtml



// var keyname = e.key; //Revisar que tecla se esta presionando
//   if (keyname === 'Enter') {
//     var comment = commentInput.value;
//     var currentComments = commentContainer.innerHTML;
//     var paragraph = document.createElement('p');
//     paragraph.classList.add('comment');
//     paragraph.innerHTML = comment;
//     commentContainer.appendChild(paragraph);
//     commentInput.value = '';
//   }


// var comment = document.querySelectorAll('.comment');

// console.log(comment);


//Tipos de variables
// var miVariable = 'Texto random';
// var variableString = 'Cadena de texto';
// var variableArray = ['0 texto 1', '1 texto 2', '2 texto 3', miVariable];
// var variableObjeto = {
//   llave: 'valor',
//   key2: 'valor 2',
//   identificador: 'valor',
// };
// var numero = 12;
// var nombreDeFuncion = function () {

// }

// function nombreDeFuncion() {

// }

